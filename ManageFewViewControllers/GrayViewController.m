//
//  GrayViewController.m
//  ManageFewViewControllers
//
//  Created by mac on 13.03.14.
//  Copyright (c) 2014 m-dev. All rights reserved.
//

#import "GrayViewController.h"

@interface GrayViewController ()

@end

@implementation GrayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onTapIAmGray:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:@"I am gray view controller. Hello =)" delegate:nil cancelButtonTitle:@"Yes I am Gray" otherButtonTitles: nil];
    [alertView show];
}

@end
