//
//  ViewController.m
//  ManageFewViewControllers
//
//  Created by mac on 12.03.14.
//  Copyright (c) 2014 m-dev. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, strong) UINavigationController *containerNavigationController;

@end

@implementation ViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        _containerNavigationController = [UINavigationController new];
        [_containerNavigationController.view setFrame:CGRectMake(84, 100, 600, 300)]; // The frame of contained view.
        _containerNavigationController.view.autoresizingMask = UIViewAutoresizingNone;
        _containerNavigationController.navigationBarHidden = YES;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [self.view addSubview:_containerNavigationController.view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onTapShowGrayView:(id)sender
{
    [self showViewWithId:@"GrayStoryboardID"];
}

- (IBAction)onTapShowBlueView:(id)sender
{
    [self showViewWithId:@"BlueStoryboardID"];
}

- (void)showViewWithId:(NSString *)stringId
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *vc = (UIViewController *)[sb instantiateViewControllerWithIdentifier:stringId];
    
    [_containerNavigationController setViewControllers:@[vc] animated:NO];
}

@end
